#include <iostream>
#include <string>
#include <memory>
#include <vector>

class TestClass
{
    private:
        int aa;
    public:
        TestClass()
            : aa(0)
        {
            std::cout << "TestClass() ctor" << std::endl;
        }
        TestClass(int aa)
            : aa(aa)
        {
            std::cout << "TestClass(int, string, double) ctor. aa: " << aa << std::endl;
        }
        ~TestClass()
        {
            std::cout << "TestClass() dtor. aa: " << aa << std::endl;
        }
        auto getAa()
        {
            return aa;
        }
};

int main (int argc, char **argv)
{
    auto mm = std::make_unique<TestClass>(1);
    {
        auto mm = std::make_unique<TestClass>(2);
    }
    std::unique_ptr<TestClass> ss = std::make_unique<TestClass>(3);
    { 
        std::vector <std::unique_ptr<TestClass> > v;
        for (int i = 10; i <= 15; i++) {
            v.push_back(std::make_unique<TestClass> (i));
            std::cout << "i: " << i << " v.size() : " << v.size() << std::endl;
        }
        // Does not work because the objects pushed into vector v are already destroyed!
        //v.push_back (std::make_unique<TestClass(20)>);
        //std::cout << "v.size() : " << v.size() << std::endl;

        // Does not work because the objects pushed into vector v are already destroyed!
        // for (int i = 0; i < v.size(); i++) {
        //   std::cout << "v[%d] " << i << v[i] << std::endl;
        // }

    }
}
