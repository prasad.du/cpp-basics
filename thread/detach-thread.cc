#include <iostream>
#include <thread>
#include <chrono>

void thread_fn(void)
{
    int sleep_for = 30;
    std::cout << "In thread_fn() " << std::endl;
    std::cout << "In thread_fn(). Sleeping for " << sleep_for << " seconds" << std::endl;
    std::this_thread::sleep_for (std::chrono::seconds(sleep_for));
    std::cout << "In thread_fn(). finished sleeping" << std::endl;

}

int main (int argc, char **argv)
{
    int sleep_for = 5;
    std::cout << "In main" << std::endl;
    std::thread t1(&thread_fn); 
    std::cout << "In main. detaching t1" << std::endl;
    t1.detach();
    std::cout << "In main. Sleeping for " << sleep_for << " seconds" << std::endl;
    std::this_thread::sleep_for (std::chrono::seconds(sleep_for));

    std::cout << "In main. finished sleeping" << std::endl;

    //t1.join(); // Error to join a detached thread.

    return 0;
}
