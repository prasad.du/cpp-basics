/*
* odd-even.cc : to print 1, 2, 3, 4,... 100 using 2 threads. 
* thread1 prints even number, thread2 prints odd numbers
*/

#include <iostream>
#include <thread>
#include <condition_variable>

int counter = 1;

std::mutex m;
std::condition_variable cv;
bool turn_of_odd = false;
bool turn_of_even = false;

const int max_limit = 100;


void print_odd(void)
{
    while (counter <= max_limit)
    {
        std::unique_lock<std::mutex> lk(m);
        cv.wait(lk, [] {return turn_of_odd;});
        if ((counter & 0x1) == 1)
        {
            std::cout << counter << " ";
        }
        counter++;
        turn_of_odd = false;
        turn_of_even = true;
        lk.unlock();
        cv.notify_one();
    }
    
}

void print_even(void)
{
    while (counter <= max_limit)
    {
        std::unique_lock<std::mutex> lk(m);
        cv.wait(lk, [] {return turn_of_even;});
        if ((counter & 0x1) == 0)
        {
            std::cout << counter << " ";
        }
        counter++;
        turn_of_odd = true;
        turn_of_even = false;
        lk.unlock();
        cv.notify_one();
    }
}

int main(int argc, char **argv)
{
    turn_of_odd = true;
    std::thread odd(&print_odd);
    std::thread even(&print_even);

    even.join();
    odd.join();

    return 0;
}


