CPPFLAGS := -std=c++14 -g

clean:
	rm -f uniq-ptr
	rm -f object-lifecycle/olc-simple
	rm -f object-lifecycle/olc-using-uniq-ptr
	rm -f object-lifecycle/olc-using-new
