#include <iostream>
#include <string>
#include <vector>
#include <memory>

#include "olc-using-uniq-ptr.hh"

SimpleClass::SimpleClass()
{
    ++s_objectCurrentCount;
    m_id = ++s_objectCounter;
    std::cout << "SimpleClass() ctor. m_id " << m_id 
        << " s_objectCounter: " << s_objectCounter 
        << " s_objectCurrentCount " << s_objectCurrentCount << std::endl;
}

SimpleClass::~SimpleClass()
{
    --s_objectCurrentCount;
    std::cout << "SimpleClass() dtor. m_id " << m_id 
        << " s_objectCounter: " << s_objectCounter 
        << " s_objectCurrentCount " << s_objectCurrentCount << std::endl;
}

int SimpleClass::s_objectCounter = 0;
int SimpleClass::s_objectCurrentCount = 0;

int main (int argc, char **argv)
{

    std::unique_ptr<SimpleClass> s1 = std::make_unique<SimpleClass>();
    //std::vector<SimpleClass *> vv;
    std::vector<std::unique_ptr<SimpleClass>> vv;
    for (int i = 0; i < 4; i++)
    {
        vv.push_back(std::make_unique<SimpleClass>());
    }
    SimpleClass *s2 = new SimpleClass();
    // memory leak! s2 is never dtor'ed!

    SimpleClass *s3 = new SimpleClass();
    // without explicit delete s3, this will leak memory.
    
    SimpleClass *s4 = s3; // this is ok. Also not this does not call SimpleClass ctor.


    delete s3;

    // all objects in vv and s2 are deleted now!

    std::unique_ptr<SimpleClass> s5; // just declare. Does not call ctor.
    // this is illegal!
    //s5 = s1;

    return 0;
}
