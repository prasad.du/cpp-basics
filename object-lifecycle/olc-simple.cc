#include <iostream>
#include <string>
#include <vector>

#include "olc-simple.hh"

SimpleClass::SimpleClass()
{
    ++s_objectCurrentCount;
    m_id = ++s_objectCounter;
    std::cout << "SimpleClass() ctor. m_id " << m_id 
        << " s_objectCounter: " << s_objectCounter 
        << " s_objectCurrentCount " << s_objectCurrentCount << std::endl;
}

SimpleClass::~SimpleClass()
{
    --s_objectCurrentCount;
    std::cout << "SimpleClass() dtor. m_id " << m_id 
        << " s_objectCounter: " << s_objectCounter 
        << " s_objectCurrentCount " << s_objectCurrentCount << std::endl;
}

int SimpleClass::s_objectCounter = 0;
int SimpleClass::s_objectCurrentCount = 0;

int main (int argc, char **argv)
{

  //  SimpleClass s1;
    std::vector<SimpleClass> vv;
    for (int i = 0; i < 4; i++)
    {
        vv.push_back(SimpleClass());
    }
    SimpleClass s2;
    

    return 0;
}
